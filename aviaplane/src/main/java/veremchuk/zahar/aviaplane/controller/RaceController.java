package veremchuk.zahar.aviaplane.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import veremchuk.zahar.aviaplane.dto.request.RaceRequest;
import veremchuk.zahar.aviaplane.dto.response.RaceResponse;

import veremchuk.zahar.aviaplane.service.RaceServiceNew;

import java.util.List;

@RestController
@RequestMapping("/race")
public class RaceController {

    @Autowired
    private RaceServiceNew raceService;

    @PostMapping
    public RaceResponse save(@RequestBody RaceRequest raceRequest){
        return raceService.save(raceRequest);
    }

    @GetMapping
    public List<RaceResponse> findAll(){
        return raceService.findAll();
    }

    @PutMapping
    public RaceResponse update(@RequestParam Long id,@RequestBody RaceRequest raceRequest){
        return  raceService.update(raceRequest,id);
    }

    @DeleteMapping
    public  void delete (@RequestParam Long id){
        raceService.delete(id);
    }
}

