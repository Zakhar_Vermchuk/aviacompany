package veremchuk.zahar.aviaplane.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import veremchuk.zahar.aviaplane.dto.request.PaginationRequest;
import veremchuk.zahar.aviaplane.dto.request.SquadRequest;
import veremchuk.zahar.aviaplane.dto.response.DataResponse;
import veremchuk.zahar.aviaplane.dto.response.PlaneResponse;
import veremchuk.zahar.aviaplane.dto.response.SquadResponse;
import veremchuk.zahar.aviaplane.service.SquadService;

import java.util.List;

@RestController
@RequestMapping("/squad")
public class SquadController {

    @Autowired
    private SquadService squadService;

    @PostMapping
    public SquadResponse save(@RequestBody SquadRequest squadRequest){
        return squadService.save(squadRequest);
    }

    @GetMapping
    public List<SquadResponse> findAll(){
        return squadService.findAll();
    }

    @PutMapping
    public SquadResponse update(@RequestParam Long id,@RequestBody SquadRequest squadRequest){
        return  squadService.update(squadRequest,id);
    }
    @PostMapping("/page")
    public DataResponse<SquadResponse> findPage(@RequestBody PaginationRequest paginationRequest){
        return squadService.findPage(paginationRequest);
    }

    @DeleteMapping
    public  void delete (@RequestParam Long id){
        squadService.delete(id);
    }
}
