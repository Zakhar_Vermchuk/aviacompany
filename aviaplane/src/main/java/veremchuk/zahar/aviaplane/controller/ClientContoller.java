package veremchuk.zahar.aviaplane.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import veremchuk.zahar.aviaplane.dto.request.ClientRequest;
import veremchuk.zahar.aviaplane.dto.response.ClientResponse;
import veremchuk.zahar.aviaplane.service.ClientService;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientContoller {

    @Autowired
    private ClientService clientService;

    @PostMapping
    public ClientResponse save(@RequestBody ClientRequest clientRequest){
        return clientService.save(clientRequest);
    }
    @GetMapping
    public List<ClientResponse> findAll(){
        return clientService.findAll();
    }
    @PutMapping
    public ClientResponse update(@RequestBody Long id,@RequestBody ClientRequest clientRequest){
        return  clientService.update(clientRequest,id);
    }
    @DeleteMapping
    public  void delete (@RequestBody Long id){
        clientService.delete(id);
    }
}

