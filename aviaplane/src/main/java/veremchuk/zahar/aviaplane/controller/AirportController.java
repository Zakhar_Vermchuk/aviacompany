package veremchuk.zahar.aviaplane.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import veremchuk.zahar.aviaplane.dto.request.AirportRequest;
import veremchuk.zahar.aviaplane.dto.request.PaginationRequest;
import veremchuk.zahar.aviaplane.dto.response.AirportResponse;

import veremchuk.zahar.aviaplane.dto.response.DataResponse;
import veremchuk.zahar.aviaplane.dto.response.SquadResponse;
import veremchuk.zahar.aviaplane.service.AirportService;

import java.util.List;

@RestController
@RequestMapping("/airport")
public class AirportController {

    @Autowired
    private AirportService airportService;

    @PostMapping
    public AirportResponse save(@RequestBody AirportRequest airportRequest){
        return airportService.save(airportRequest);
    }
    @GetMapping
    public List<AirportResponse> findAll(){
        return airportService.findAll();
    }
    @PutMapping
    public AirportResponse update(@RequestParam Long id,@RequestBody AirportRequest airportRequest){
        return  airportService.update(airportRequest,id);
    }
    @DeleteMapping
    public  void delete (@RequestParam Long id){
        airportService.delete(id);
    }

}



