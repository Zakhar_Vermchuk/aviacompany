package veremchuk.zahar.aviaplane.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import veremchuk.zahar.aviaplane.dto.request.TicketRequest;
import veremchuk.zahar.aviaplane.dto.response.TicketResponse;
import veremchuk.zahar.aviaplane.service.TicketService;

import java.util.List;

@RestController
@RequestMapping("/ticket")
public class TicketController {


    @Autowired
    private TicketService ticketService;

    @PostMapping
    public TicketResponse save(@RequestBody TicketRequest ticketRequest){
        return ticketService.save(ticketRequest);
    }

    @GetMapping
    public List<TicketResponse> findAll(){
        return ticketService.findALL();
    }

    @PutMapping
    public TicketResponse update(@RequestParam Long id,@RequestBody TicketRequest ticketRequest){
        return  ticketService.update(ticketRequest,id);
    }

    @DeleteMapping
    public  void delete (@RequestParam Long id){
        ticketService.delete(id);
    }
}

