package veremchuk.zahar.aviaplane.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import veremchuk.zahar.aviaplane.dto.request.PaginationRequest;
import veremchuk.zahar.aviaplane.dto.request.PlaneRequest;
import veremchuk.zahar.aviaplane.dto.response.DataResponse;
import veremchuk.zahar.aviaplane.dto.response.PlaneResponse;
import veremchuk.zahar.aviaplane.service.PlaneService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/plane")
public class PlaneController{

    @Autowired
    private PlaneService planeService;

    @PostMapping
    public PlaneResponse save(@RequestBody PlaneRequest planeRequest)throws IOException {
        return planeService.save(planeRequest);
    }
    @PostMapping("/page")
    public DataResponse<PlaneResponse> findPage(@RequestBody PaginationRequest paginationRequest){
        return planeService.findPage(paginationRequest);
    }

    @GetMapping
    public List<PlaneResponse> findAll(){
        return planeService.findAll();
    }

    @GetMapping("/filter")
    public DataResponse<PlaneResponse> filter(@RequestParam(required = false)String name,
                                              @RequestParam Integer page,
                                              @RequestParam Integer size,
                                              @RequestParam String fieldName,
                                              @RequestParam Sort.Direction direction){
       return  planeService.getPageByFilter(name,page,size,fieldName,direction);

}

    @PutMapping
    public PlaneResponse update(@RequestParam Long id,@RequestBody PlaneRequest planeRequest)throws IOException{
        return  planeService.update(planeRequest,id);
    }

    @DeleteMapping
    public  void delete (@RequestParam Long id){
        planeService.delete(id);
    }
}
