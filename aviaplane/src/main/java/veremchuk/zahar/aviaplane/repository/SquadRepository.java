package veremchuk.zahar.aviaplane.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import veremchuk.zahar.aviaplane.entity.Squad;
@Repository
public interface SquadRepository extends JpaRepository<Squad,Long>, JpaSpecificationExecutor<Squad> {


}
