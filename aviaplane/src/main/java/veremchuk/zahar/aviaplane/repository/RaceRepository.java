package veremchuk.zahar.aviaplane.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import veremchuk.zahar.aviaplane.entity.Race;
@Repository
public interface RaceRepository extends JpaRepository<Race,Long>, JpaSpecificationExecutor<Race> {
}
