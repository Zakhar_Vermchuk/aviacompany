package veremchuk.zahar.aviaplane.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import veremchuk.zahar.aviaplane.entity.Plane;
import veremchuk.zahar.aviaplane.entity.Ticket;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository  extends JpaRepository<Ticket,Long> {


}
