package veremchuk.zahar.aviaplane.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import veremchuk.zahar.aviaplane.entity.Plane;
@Repository
public interface PlaneRepository extends JpaRepository<Plane,Long>, JpaSpecificationExecutor<Plane> {
}
