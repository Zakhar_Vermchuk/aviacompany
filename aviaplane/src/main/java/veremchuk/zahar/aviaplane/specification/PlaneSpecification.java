package veremchuk.zahar.aviaplane.specification;

import org.springframework.data.jpa.domain.Specification;
import veremchuk.zahar.aviaplane.entity.Plane;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PlaneSpecification implements Specification<Plane> {
    private String name;


    public PlaneSpecification(String name) {
        this.name = name;
    }

    @Override
    public Predicate toPredicate(Root<Plane> r, CriteriaQuery<?> query, CriteriaBuilder cb) {
       return   cb.like(r.get("name"), "%" + name + "%");
    }


}
