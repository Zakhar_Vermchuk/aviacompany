package veremchuk.zahar.aviaplane.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class AirportRequest {

    private String country;
    private String town;
    private String nameOfAirport;
}
