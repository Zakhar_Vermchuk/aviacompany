package veremchuk.zahar.aviaplane.dto.response;

import lombok.Getter;
import lombok.Setter;
import veremchuk.zahar.aviaplane.entity.Airport;
@Getter @Setter
public class AirportResponse {
    private Long id;
    private String country,town;
    private String nameOfAirport;


    public AirportResponse (Airport airport){
        id = airport.getId();
        country = airport.getCountry();
        town = airport.getTown();
        nameOfAirport = airport.getNameOfAirport();
    }

}
