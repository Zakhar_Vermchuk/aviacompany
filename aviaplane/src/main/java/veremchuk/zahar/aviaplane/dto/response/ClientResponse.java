package veremchuk.zahar.aviaplane.dto.response;

import lombok.Getter;
import lombok.Setter;
import veremchuk.zahar.aviaplane.entity.Client;

@Getter
@Setter
public class ClientResponse {
    private Long id;
    private String name;
    private String surname;
    private String address;
    private Integer number;
    private String mail;

    public ClientResponse(Client client){
        id = client.getId();
        name = client.getName();
        surname = client.getSurname();
        address = client.getAdress();
        number = client.getNumber();
        mail  = client.getMail();
    }
}
