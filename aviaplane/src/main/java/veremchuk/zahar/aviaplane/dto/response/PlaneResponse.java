package veremchuk.zahar.aviaplane.dto.response;

import lombok.Getter;
import lombok.Setter;
import veremchuk.zahar.aviaplane.entity.Plane;

@Getter @Setter
public class PlaneResponse {

    private Long id;
    private String name;
    private Integer numberOfPlace;
    private String rangeOfFlight;
    private Double speed;
    private SquadResponse squadResponse;

    public PlaneResponse(Plane plane){
        id = plane.getId();
        name = plane.getName();
        numberOfPlace = plane.getNumberOfPlace();
        rangeOfFlight = plane.getRangeOfFligt();
        speed = plane.getSpeed();
        squadResponse = new SquadResponse(plane.getSquad());
    }
}