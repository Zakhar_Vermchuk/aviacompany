package veremchuk.zahar.aviaplane.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SquadRequest {

    private String firstPilot;
    private String secondPilot;
    private String firstSteward;
    private String secondSteward;

}
