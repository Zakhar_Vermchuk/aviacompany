package veremchuk.zahar.aviaplane.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ClientRequest {
    private Long id;
    private String name;
    private String surname;
    private String address;
    private Integer number;
    private String mail;
}
