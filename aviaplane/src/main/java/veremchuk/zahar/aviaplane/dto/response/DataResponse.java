package veremchuk.zahar.aviaplane.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class DataResponse<T> {

    private Integer totalPages;
    private Long total;
    private List<T> content;

    public DataResponse(Integer totalPages, Long total, List<T> content) {
        this.totalPages = totalPages;
        this.total = total;
        this.content = content;
    }
}
