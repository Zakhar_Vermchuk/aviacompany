package veremchuk.zahar.aviaplane.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import veremchuk.zahar.aviaplane.entity.Client;
import veremchuk.zahar.aviaplane.entity.Plane;
import veremchuk.zahar.aviaplane.entity.Race;

import javax.persistence.ManyToOne;
import java.sql.Date;
@Getter
@Setter
@NoArgsConstructor
public class TicketRequest {
    private Integer passengerPlace;
    private Double weightOfThings;
    private Double priceOfTicket;


    private Long planeId;

    private Long clientId;

    private Long raceId;



}
