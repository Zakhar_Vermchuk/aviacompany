package veremchuk.zahar.aviaplane.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import veremchuk.zahar.aviaplane.entity.Squad;

@Getter @Setter
public class SquadResponse {
    private  Long id;
    private  String firstPilot;
    private String secondPilot;
    private String firstSteward;
    private String secondSteward;


    public SquadResponse(Squad squad){
        id = squad.getId();
        firstPilot = squad.getFirstPilot();
        secondPilot = squad.getSecondPilot();
        firstSteward = squad.getFirstSteward();
        secondSteward = squad.getSecondSteward();
    }

}
