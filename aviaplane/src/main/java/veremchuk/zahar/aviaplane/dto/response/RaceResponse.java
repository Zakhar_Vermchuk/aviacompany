package veremchuk.zahar.aviaplane.dto.response;

import lombok.Getter;
import lombok.Setter;
import veremchuk.zahar.aviaplane.entity.Race;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalTime;

@Getter @Setter
public class RaceResponse {
    private Time timeOfFlight;

    private AirportResponse airportResponseStartOfFlight;

    private AirportResponse airportResponseFinishOfFlight;

    public RaceResponse(Race race){
        timeOfFlight = race.getTimeOfFlight ();
        airportResponseStartOfFlight = new AirportResponse(race.getAirportStart());
        airportResponseFinishOfFlight = new AirportResponse(race.getAirportFINISH());
    }

}
