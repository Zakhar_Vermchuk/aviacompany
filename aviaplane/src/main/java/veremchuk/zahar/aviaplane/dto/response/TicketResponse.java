package veremchuk.zahar.aviaplane.dto.response;

import lombok.Getter;
import lombok.Setter;
import veremchuk.zahar.aviaplane.entity.Ticket;

import java.sql.Date;

@Getter @Setter
public class TicketResponse {
    private Integer passengerPlace;
    private Double weightOfThings;
    private Double priceOfTicket;

    private PlaneResponse planeResponse;

    private ClientResponse clientResponse;

    private RaceResponse raceResponse;



    public TicketResponse(Ticket ticket){
        passengerPlace = ticket.getPassengerPlace();
        weightOfThings = ticket.getWeightOfThings();
        priceOfTicket = ticket.getPriceOfTicket();
        planeResponse = new PlaneResponse(ticket.getPlane());
        clientResponse = new ClientResponse(ticket.getClient());
        raceResponse = new RaceResponse(ticket.getRace());

    }
}
