package veremchuk.zahar.aviaplane.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter @Setter
@NoArgsConstructor
public class PlaneRequest {
    private  String name;
    private Integer numberOfPlace;
    private String rangeOfFlight;
    private Double speed;
    @NotNull
    private Long squadId;


}
