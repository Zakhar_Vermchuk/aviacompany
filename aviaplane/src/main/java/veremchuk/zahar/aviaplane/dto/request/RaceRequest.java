package veremchuk.zahar.aviaplane.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
public class RaceRequest {
    private Time timeOfFlight;

    private Long airportStartId;

    private Long airportFinishId;
}
