package veremchuk.zahar.aviaplane.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import veremchuk.zahar.aviaplane.dto.request.RaceRequest;
import veremchuk.zahar.aviaplane.dto.response.RaceResponse;
import veremchuk.zahar.aviaplane.entity.Airport;
import veremchuk.zahar.aviaplane.entity.Race;
import veremchuk.zahar.aviaplane.exception.WrongInputDataException;
import veremchuk.zahar.aviaplane.repository.AirportRepository;
import veremchuk.zahar.aviaplane.repository.RaceRepository;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class RaceServiceNew {
    @Autowired
    private RaceRepository racerepository;

    @Autowired
    private AirportService airportService;

    public Race findOne(Long id){
        return racerepository.findById(id).
                orElseThrow(()-> new WrongInputDataException("Race with id "+ id +"not exist"));
    }
    public void delete(Long id){racerepository.delete(findOne(id));}

    public  List<RaceResponse> findAll(){
        return racerepository.findAll().
                stream().map(RaceResponse::new).collect(Collectors.toList());
    }

    public RaceResponse save(RaceRequest raceRequest){
        return new RaceResponse(racerepository.save(raceRequestToRace(raceRequest,null)));
    }
    public RaceResponse update(RaceRequest raceRequest,Long id){
        Race race = raceRequestToRace(raceRequest, findOne(id));
        return new RaceResponse(racerepository.save(race));
    }

    private Race raceRequestToRace(RaceRequest raceRequest,Race race){
        if(race == null){
            race = new Race();
        }

        race.setTimeOfFlight(raceRequest.getTimeOfFlight());

        if(raceRequest.getAirportStartId() != null){
            Airport airoportStart = airportService.findOne(raceRequest.getAirportStartId());
            race.setAirportStart(airoportStart);}

        if(raceRequest.getAirportFinishId() != null){
            Airport airoportFinish = airportService.findOne(raceRequest.getAirportFinishId());
            race.setAirportFINISH(airoportFinish);}

      return race;
    }
}
