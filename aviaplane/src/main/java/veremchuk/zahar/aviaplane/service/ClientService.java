package veremchuk.zahar.aviaplane.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import veremchuk.zahar.aviaplane.dto.request.ClientRequest;
import veremchuk.zahar.aviaplane.dto.response.ClientResponse;
import veremchuk.zahar.aviaplane.entity.Client;
import veremchuk.zahar.aviaplane.exception.WrongInputDataException;
import veremchuk.zahar.aviaplane.repository.ClientRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Client findOne(Long id){
        return clientRepository.findById(id).
                orElseThrow(()-> new WrongInputDataException("Client with id " + id + " not exist"));
    }

    private Client clientRequestToClient(ClientRequest clientRequest,Client client){
        if(client == null){
            client = new Client();
        }
        client.setName(clientRequest.getName());
        client.setSurname(clientRequest.getSurname());
        client.setAdress(clientRequest.getAddress());
        client.setMail(clientRequest.getMail());
        client.setNumber(clientRequest.getNumber());

        return client;
    }

    public ClientResponse save(ClientRequest clientRequest){
        return new ClientResponse(clientRepository.save(clientRequestToClient(clientRequest,null)));
    }
    public ClientResponse update(ClientRequest clientRequest,Long id){
        Client client = clientRequestToClient(clientRequest, findOne(id));
        return new ClientResponse(clientRepository.save(client));
    }
    public void delete(Long id){
        clientRepository.delete(findOne(id));
    }

    public List<ClientResponse> findAll(){
        return clientRepository.findAll().stream().map(ClientResponse::new).collect(Collectors.toList());
    }
}
