package veremchuk.zahar.aviaplane.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import veremchuk.zahar.aviaplane.dto.request.PaginationRequest;
import veremchuk.zahar.aviaplane.dto.request.SquadRequest;

import veremchuk.zahar.aviaplane.dto.response.ClientResponse;
import veremchuk.zahar.aviaplane.dto.response.DataResponse;
import veremchuk.zahar.aviaplane.dto.response.SquadResponse;
import veremchuk.zahar.aviaplane.entity.Squad;
import veremchuk.zahar.aviaplane.exception.WrongInputDataException;
import veremchuk.zahar.aviaplane.repository.SquadRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SquadService {
    @Autowired
    private SquadRepository squadRepository;

    public SquadResponse save(SquadRequest squadRequest){
        return  new SquadResponse(squadRepository.save(squadRequestToSquad(squadRequest,null)));
    }
    public List<SquadResponse> findAll(){
        return squadRepository.findAll().stream().
                map(SquadResponse::new).collect(Collectors.toList());
    }

    public Squad findOne(Long id){
        return squadRepository.findById(id).orElseThrow(()->
        new WrongInputDataException("Category with id"+ id + "not exist"));
    }

    public Squad squadRequestToSquad(SquadRequest squadRequest,Squad squad){
        if(squad == null){
            squad = new Squad();
        }
        squad.setFirstPilot(squadRequest.getFirstPilot());
        squad.setSecondPilot(squadRequest.getSecondPilot());
        squad.setFirstSteward(squadRequest.getFirstSteward());
        squad.setSecondSteward(squadRequest.getSecondSteward());

        return squad;
    }
    public DataResponse<SquadResponse> findPage(PaginationRequest paginationRequest ){
        Page<Squad> page = squadRepository.findAll(paginationRequest.toPageable());
        List<SquadResponse> collect = page.get().map(SquadResponse::new).collect(Collectors.toList());
        return new DataResponse<>(page.getTotalPages(),page.getTotalElements(),collect);


    }
    public void delete(Long id){
        squadRepository.delete(findOne(id));
    }
    public SquadResponse update(SquadRequest  squadRequest,Long id){
        Squad squad = squadRequestToSquad(squadRequest, findOne(id));
        return new SquadResponse(squadRepository.save(squad));

    }

}
