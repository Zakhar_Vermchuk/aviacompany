package veremchuk.zahar.aviaplane.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import veremchuk.zahar.aviaplane.dto.request.TicketRequest;

import veremchuk.zahar.aviaplane.dto.response.TicketResponse;
import veremchuk.zahar.aviaplane.entity.*;
import veremchuk.zahar.aviaplane.exception.WrongInputDataException;
import veremchuk.zahar.aviaplane.repository.TicketRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private PlaneService planeService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private RaceServiceNew raceService;


    public Ticket findOne(Long id){
        return ticketRepository.findById(id).
                orElseThrow(()->new WrongInputDataException("Ticket with id "+ id + "not exist"));
    }

    private Ticket ticketRequestToTicket(TicketRequest ticketRequest,Ticket ticket){
        if(ticket == null){
            ticket = new Ticket();
        }
        ticket.setPassengerPlace(ticketRequest.getPassengerPlace());
        ticket.setWeightOfThings(ticketRequest.getWeightOfThings());
        ticket.setPriceOfTicket(ticketRequest.getPriceOfTicket());

        Plane plane = planeService.findOne(ticketRequest.getPlaneId());
        Client client = clientService.findOne(ticketRequest.getClientId());
        Race race = raceService.findOne(ticketRequest.getRaceId());


        ticket.setPlane(plane);
        ticket.setClient(client);
        ticket.setRace(race);



        return  ticket;
    }

    public List<TicketResponse> findALL(){
        return ticketRepository.findAll().stream().map(TicketResponse::new).collect(Collectors.toList());
    }

    public void delete(Long id){
        ticketRepository.delete(findOne(id));
    }

    public TicketResponse update(TicketRequest ticketRequest,Long id){
        Ticket ticket = ticketRequestToTicket(ticketRequest, findOne(id));
        return new TicketResponse(ticketRepository.save(ticket));
    }

    public TicketResponse save(TicketRequest ticketRequest){
        return new TicketResponse(ticketRepository.save(ticketRequestToTicket(ticketRequest,null)));
    }
}
