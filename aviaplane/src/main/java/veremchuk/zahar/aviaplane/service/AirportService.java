package veremchuk.zahar.aviaplane.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import veremchuk.zahar.aviaplane.dto.request.AirportRequest;
import veremchuk.zahar.aviaplane.dto.response.AirportResponse;
import veremchuk.zahar.aviaplane.entity.Airport;
import veremchuk.zahar.aviaplane.exception.WrongInputDataException;
import veremchuk.zahar.aviaplane.repository.AirportRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AirportService {
    @Autowired
    private AirportRepository airportRepository;

    public AirportResponse save(AirportRequest airportRequest){
       return new AirportResponse
               (airportRepository.save(airportRequestToAirport(airportRequest,null)));
    }

    public List<AirportResponse> findAll(){
        return airportRepository.findAll().stream().
                map(AirportResponse::new).collect(Collectors.toList());
    }

    public void delete(Long id){airportRepository.delete(findOne(id));}

    public Airport findOne(Long id){
        return  airportRepository.findById(id).
                orElseThrow(()-> new WrongInputDataException("Airport with id"+ id +"not exist"));
    }

    public AirportResponse update(AirportRequest airportRequest,Long id){
        Airport airport = airportRequestToAirport(airportRequest, findOne(id));
        return  new AirportResponse(airportRepository.save(airport));
    }


    private Airport airportRequestToAirport(AirportRequest airportRequest,Airport airport){
        if(airport == null){
            airport = new Airport();
        }
        airport.setTown(airportRequest.getTown());
        airport.setCountry(airportRequest.getCountry());
        airport.setNameOfAirport(airportRequest.getNameOfAirport());

        return airport;
    }
}
