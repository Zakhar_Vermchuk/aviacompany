package veremchuk.zahar.aviaplane.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import veremchuk.zahar.aviaplane.dto.request.PaginationRequest;
import veremchuk.zahar.aviaplane.dto.request.PlaneRequest;
import veremchuk.zahar.aviaplane.dto.response.DataResponse;
import veremchuk.zahar.aviaplane.dto.response.PlaneResponse;
import veremchuk.zahar.aviaplane.entity.Plane;
import veremchuk.zahar.aviaplane.entity.Squad;
import veremchuk.zahar.aviaplane.exception.WrongInputDataException;
import veremchuk.zahar.aviaplane.repository.PlaneRepository;
import veremchuk.zahar.aviaplane.specification.PlaneSpecification;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaneService {

    @Autowired
    private PlaneRepository planeRepository;

    @Autowired
    private SquadService squadService;

    @Autowired
    private FileService fileService;

    public PlaneResponse save(PlaneRequest  planeRequest)throws IOException{
        return new PlaneResponse
                (planeRepository.save( planeRequestToPlane(planeRequest, null)));
    }
    public List<PlaneResponse> findAll(){
        return planeRepository.findAll().stream().map(PlaneResponse::new).collect(Collectors.toList());
    }
    public PlaneResponse update(PlaneRequest planeRequest,Long id)throws IOException{
        Plane plane = planeRequestToPlane(planeRequest, findOne(id));
        return new PlaneResponse(planeRepository.save(plane));
    }
    public void delete(Long id){
        planeRepository.delete(findOne(id));
    }

    public Plane findOne(Long id){
        return  planeRepository.findById(id).orElseThrow
                (()-> new WrongInputDataException("Plane with id " + id +"not exist "));
    }

    private  Plane planeRequestToPlane(PlaneRequest planeRequest,Plane plane) throws WrongInputDataException, IOException {
        if(plane == null){
            plane  = new Plane();
        }
        plane.setName(planeRequest.getName());
        plane.setNumberOfPlace(planeRequest.getNumberOfPlace());
        plane.setRangeOfFligt(planeRequest.getRangeOfFlight());
        plane.setSpeed(planeRequest.getSpeed());

        if(planeRequest.getSquadId() != null) {
            Squad squad = squadService.findOne(planeRequest.getSquadId());
            plane.setSquad(squad);
        }

        return  plane;
    }

    public DataResponse<PlaneResponse> getPageByFilter(String name, Integer page, Integer size, String fieldName, Sort.Direction direction) {
        Page<Plane> all = planeRepository.findAll(new PlaneSpecification(name),
                PageRequest.of(page, size, direction, fieldName)
        );

        List<PlaneResponse> collect = all.get().map(PlaneResponse::new).collect(Collectors.toList());
        return  new DataResponse<>(all.getTotalPages(),all.getTotalElements(),collect);
    }

    public DataResponse<PlaneResponse> findPage(PaginationRequest paginationRequest){
        Page<Plane> page = planeRepository.findAll(paginationRequest.toPageable());
        List<PlaneResponse> collect = page.get().map(PlaneResponse::new).collect(Collectors.toList());
        return new DataResponse<>(page.getTotalPages(),page.getTotalElements(),collect);

    }
}
