package veremchuk.zahar.aviaplane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import veremchuk.zahar.aviaplane.entity.Client;
import veremchuk.zahar.aviaplane.entity.Race;
import veremchuk.zahar.aviaplane.entity.Ticket;
import veremchuk.zahar.aviaplane.repository.*;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class AviaplaneApplication {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AirportRepository airportRepository;
    @Autowired
    private PlaneRepository planeRepository;
    @Autowired
    private RaceRepository raceRepository;
    @Autowired
    private SquadRepository squadRepository;
    @Autowired
    private TicketRepository ticketRepository;
 

    @PostConstruct
    public void  post(){
    }
    public static void main(String[] args) {
        SpringApplication.run(AviaplaneApplication.class, args);
    }

}
