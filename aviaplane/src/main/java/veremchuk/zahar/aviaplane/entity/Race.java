package veremchuk.zahar.aviaplane.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Race{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Time timeOfFlight;
    @OneToMany(mappedBy = "race")
    private List<Ticket> tickets = new ArrayList<>();
    @ManyToOne
    private Airport airportStart;
    @ManyToOne
    private Airport airportFINISH;

    @Override
    public String toString() {
        return "Race{" +
                "id=" + id +
                ", timeOfFlight=" + timeOfFlight +
                '}';
    }
}
