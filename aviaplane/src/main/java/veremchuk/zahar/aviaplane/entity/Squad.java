package veremchuk.zahar.aviaplane.entity;

import lombok.*;

import javax.persistence.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Squad{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstPilot;
    private String secondPilot;
    private String firstSteward;
    private String secondSteward;

    @OneToOne(mappedBy = "squad")
    private Plane plane;

    @Override
    public String toString() {
        return "Squad{" +
                "id=" + id +
                ", firstPilot='" + firstPilot + '\'' +
                ", secondPilot='" + secondPilot + '\'' +
                ", firstSteward='" + firstSteward + '\'' +
                ", secondSteward='" + secondSteward + '\'' +
                ", plane=" + plane +
                '}';
    }
}
