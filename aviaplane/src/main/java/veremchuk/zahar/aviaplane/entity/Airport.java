package veremchuk.zahar.aviaplane.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity

public class Airport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String country;
    private String town;
    private String nameOfAirport;
    @OneToMany(mappedBy = "airportStart")
    private List<Race> races = new ArrayList<>();

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", town='" + town + '\'' +
                ", nameOfAirport='" + nameOfAirport + '\'' +
                '}';
    }
}
