package veremchuk.zahar.aviaplane.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Ticket{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer passengerPlace;
    private Double weightOfThings;
    private Double priceOfTicket;

    @ManyToOne
    private Plane plane;
    @ManyToOne
    private Client client;
    @ManyToOne
    private Race race;


    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", passengerPlace=" + passengerPlace +
                ", weightOfThings=" + weightOfThings +
                ", priceOfTicket=" + priceOfTicket +
                ", plane=" + plane +
                ", client=" + client +
                ", race=" + race +
                '}';
    }
}

