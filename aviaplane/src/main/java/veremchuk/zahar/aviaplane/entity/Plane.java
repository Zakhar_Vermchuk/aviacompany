package veremchuk.zahar.aviaplane.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Plane{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer numberOfPlace;
    private String rangeOfFligt;
    private Double speed;

    @OneToMany(mappedBy = "plane")
    private List<Ticket> tickets = new ArrayList<>();

    @OneToOne
    private Squad squad;

}
